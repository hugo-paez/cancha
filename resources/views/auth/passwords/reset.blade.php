@extends('template.admin')

@section('title', trans('auth.reset_password'))

@section('content')
  <form class="form-horizontal" role="form" method="POST" action="{{ url('/password/reset') }}">
    {{ csrf_field() }}

    <input type="hidden" name="token" value="{{ $token }}">

    <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
      {!! Form::label('email', trans('auth.email'), ['class' => 'col-md-4 control-label']) !!}

      <div class="col-md-6">
        {!! Form::email('email', $email, ['class' => 'form-control', 'placeholder' => trans('auth.ph_email'), 'required']) !!}

        @if ($errors->has('email'))
          <span class="help-block">
            <strong>{{ $errors->first('email') }}</strong>
          </span>
        @endif
      </div>
    </div>

    <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
      {!! Form::label('password', trans('auth.password'), ['class' => 'col-md-4 control-label']) !!}

      <div class="col-md-6">
        {!! Form::password('password', ['class' => 'form-control', 'placeholder' => trans('auth.ph_password'), 'required']) !!}

        @if ($errors->has('password'))
          <span class="help-block">
            <strong>{{ $errors->first('password') }}</strong>
          </span>
        @endif
      </div>
    </div>

    <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
      {!! Form::label('password-confirm', trans('auth.password_confirmation'), ['class' => 'col-md-4 control-label']) !!}

      <div class="col-md-6">
        {!! Form::password('password_confirmation', ['class' => 'form-control', 'placeholder' => trans('auth.ph_password'), 'required']) !!}

        @if ($errors->has('password_confirmation'))
          <span class="help-block">
            <strong>{{ $errors->first('password_confirmation') }}</strong>
          </span>
        @endif
      </div>
    </div>

    <div class="form-group">
      <div class="col-md-6 col-md-offset-4">
        <button type="submit" class="btn btn-primary">
          <i class="fa fa-btn fa-refresh"></i> {{ trans('auth.reset_password') }}
        </button>
      </div>
    </div>
  </form>
@endsection
