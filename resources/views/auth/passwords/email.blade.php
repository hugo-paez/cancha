@extends('template.admin')

@section('title', trans('auth.reset_password'))

<!-- Main Content -->
@section('content')
  @if (session('status'))
    <div class="alert alert-success">
      {{ session('status') }}
    </div>
  @endif

  <form class="form-horizontal" role="form" method="POST" action="{{ url('/password/email') }}">
    {{ csrf_field() }}

    <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
      {!! Form::label('email', trans('auth.email'), ['class' => 'col-md-4 control-label']) !!}

      <div class="col-md-6">
        {!! Form::email('email', old('email'), ['class' => 'form-control', 'placeholder' => trans('auth.ph_email'), 'required']) !!}

        @if ($errors->has('email'))
          <span class="help-block">
            <strong>{{ $errors->first('email') }}</strong>
          </span>
        @endif
      </div>
    </div>

    <div class="form-group">
      <div class="col-md-6 col-md-offset-4">
        <button type="submit" class="btn btn-primary">
          <i class="fa fa-btn fa-envelope"></i> {{ trans('auth.recover_password') }}
        </button>
      </div>
    </div>
  </form>
@endsection
