@extends('template.admin')

@section('title', trans('general.insert_type'))

@section('content')
  <!-- Form -->
  {!! Form::open(['route' => 'types.store', 'method' => 'POST', 'class' => 'form-horizontal']) !!}
    <div class="form-group">
      {!! Form::label('name', trans('general.name'), ['class' => 'col-sm-2 control-label']) !!}
      <div class="col-sm-10">
        {!! Form::text('name', old('name'), ['class' => 'form-control', 'placeholder' => trans('general.ph_name_type'), 'required']) !!}
      </div>
    </div>

    <div class="form-group text-center">
      {!! Form::submit(trans('general.insert_type'), ['class' => 'btn btn-primary']) !!}
    </div>
  {!! Form::close() !!}
@endsection
