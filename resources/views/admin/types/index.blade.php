@extends('template.admin')

@section('title', trans('general.list_types'))

@section('content')
  <!-- Search -->
  {!! Form::model(Request::all(), ['route' => 'types.index', 'method' => 'GET', 'class' => 'navbar-form']) !!}
    <div class='navbar-left'>
      <div class='input-group'>
        {!! Form::text('name', null, ['class' => 'form-control', 'placeholder' => trans('general.search_type'), 'aria-describedby' => 'search']) !!}
        <span class="input-group-btn">
          {!! Form::button("<span class='glyphicon glyphicon-search' aria-hidden='true'>", array('class' => 'btn btn-search', 'type' => 'submit')) !!}
        </span>
      </div>
    </div>

    <div class='navbar-right'>
      <div class="input-group">
        <a href="{!! route('types.create') !!}" class="btn btn-info">{!! trans('general.insert_type') !!}</a>
      </div>
    </div>
  {!! Form::close() !!}
  <br>
  <hr>

  <!-- Content -->
  <div class="table-responsive">
    <table class="table table-striped">
      <thead>
        <th>{!! trans('general.id') !!}</th>
        <th>{!! trans('general.name') !!}</th>
        <th>{!! trans('general.action') !!}</th>
      </thead>
      <tbody>
        @foreach($types as $type)
          <tr>
            <td>{!! $type->id !!}</td>
            <td>{!! $type->name !!}</td>
            <td>
              <a href="{!! route('types.edit', $type->id) !!}" class="btn btn-warning" data-toggle="tooltip" title="{!! trans('general.tt_edit', ['name' => $type->name]) !!}"><span class="glyphicon glyphicon-wrench" aria-hidden="true"></span></a>
              <a href="" class="btn btn-danger" data-toggle="modal" data-target="#modalDelete" data-name="{!! $type->name !!}" data-id="{!! $type->id !!}" title="{!! trans('general.tt_delete', ['name' => $type->name]) !!}"><span class="glyphicon glyphicon-remove-circle" aria-hidden="true"></span></a>
            </td>
          </tr>
        @endforeach
      </tbody>
    </table>
    <div class="text-center">
      {!! $types->appends(Request::all())->render() !!}
    </div>
  </div>

  <!-- Modal -->
  <div class="modal fade" id="modalDelete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title" id="myModalLabel"></h4>
        </div>
        <div class="modal-body">

        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">{!! trans('general.close') !!}</button>
          <a class="btn btn-danger" href="" id="link">{!! trans('general.delete') !!}</a>
        </div>
      </div>
    </div>
  </div>
@endsection

@section('javascript')
  <script type="text/javascript">
    $('#modalDelete').on('show.bs.modal', function (event) {
      var button = $(event.relatedTarget);
      var name = button.data('name');
      var id = button.data('id');

      var modal = $(this);
      modal.find('.modal-title').text("{!! trans('general.delete_title', ['name' => '" + name + "']) !!}");
      modal.find('.modal-body').text("{!! trans('general.delete_text', ['name' => '" + name + "', 'type' => 'el tipo de evento']) !!}");

      var url = "/types/" + id + "/destroy";

      $("#link").attr("href", url);
    });

    $(function () {
      $('[data-toggle="tooltip"]').tooltip()
      $('[data-toggle="modal"]').tooltip()
    })
  </script>
@endsection
