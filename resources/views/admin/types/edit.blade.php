@extends('template.admin')

@section('title', trans('general.edit_type') . ' ' . $type->name)

@section('content')
  <!-- Form -->
  {!! Form::open(['route' => ['types.update', $type], 'method' => 'PUT', 'class' => 'form-horizontal']) !!}
    <div class="form-group">
      {!! Form::label('name', trans('general.name'), ['class' => 'col-sm-2 control-label']) !!}
      <div class="col-sm-10">
        {!! Form::text('name', $type->name, ['class' => 'form-control', 'placeholder' => trans('general.ph_name_type'), 'required']) !!}
      </div>
    </div>

    <div class="form-group text-center">
      {!! Form::submit(trans('general.edit_type'), ['class' => 'btn btn-primary']) !!}
    </div>
  {!! Form::close() !!}
@endsection
