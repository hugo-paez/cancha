@extends('template.admin')

@section('title', trans('general.insert_city'))

@section('css')
  {!! Html::style('plugins/select2/css/select2.min.css') !!}
@endsection

@section('content')
  <!-- Form -->
  {!! Form::open(['route' => 'cities.store', 'method' => 'POST', 'class' => 'form-horizontal']) !!}
    <div class="form-group">
      {!! Form::label('name', trans('general.name'), ['class' => 'col-sm-2 control-label']) !!}
      <div class="col-sm-10">
        {!! Form::text('name', old('name'), ['class' => 'form-control', 'placeholder' => trans('general.ph_name_city'), 'required']) !!}
      </div>
    </div>

    <div class="form-group">
      {!! Form::label('region_id', trans('general.region'), ['class' => 'col-sm-2 control-label']) !!}
      <div class="col-sm-10">
        {!! Form::select('region_id', $regions, old('region_id'), ['class' => 'form-control', 'placeholder' => trans('general.ph_region'), 'required']) !!}
      </div>
    </div>

    <div class="form-group text-center">
      {!! Form::submit(trans('general.insert_city'), ['class' => 'btn btn-primary']) !!}
    </div>
  {!! Form::close() !!}
@endsection

@section('javascript')
  {!! Html::script('plugins/select2/js/select2.min.js') !!}
  <script type="text/javascript">
    $(document).ready(function() {
    	$("#region_id").select2({
        placeholder: "{!! trans('general.ph_region') !!}",
        allowClear: true
      });
    });
  </script>
@endsection
