@extends('template.admin')

@section('title', trans('general.list_cities'))

@section('content')
  <!-- Search -->
  {!! Form::model(Request::all(), ['route' => 'cities.index', 'method' => 'GET', 'class' => 'navbar-form']) !!}
    <div class='navbar-left'>
      <div class='input-group'>
        {!! Form::text('name', null, ['class' => 'form-control', 'placeholder' => trans('general.search_city'), 'aria-describedby' => 'search']) !!}
        <span class="input-group-btn">
          {!! Form::button("<span class='glyphicon glyphicon-search' aria-hidden='true'>", array('class' => 'btn btn-search', 'type' => 'submit')) !!}
        </span>
      </div>
    </div>

    <div class='navbar-right'>
      <div class="input-group">
        <a href="{!! route('cities.create') !!}" class="btn btn-info">{!! trans('general.insert_city') !!}</a>
      </div>
    </div>
  {!! Form::close() !!}
  <br>
  <hr>

  <!-- Content -->
  <div class="table-responsive">
    <table class="table table-hover">
      <thead>
        <th>{!! trans('general.id') !!}</th>
        <th>{!! trans('general.name') !!}</th>
        <th>{!! trans('general.region') !!}</th>
        <th>{!! trans('general.action') !!}</th>
      </thead>
      <tbody>
        @foreach($cities as $city)
          <tr>
            <td>{!! $city->id !!}</td>
            <td>{!! $city->name !!}</td>
            <td>{!! $city->region->name !!}</td>
            <td>
              <a href="{!! route('cities.edit', $city->id) !!}" class="btn btn-warning" data-toggle="tooltip" title="{!! trans('general.tt_edit', ['name' => $city->name]) !!}"><span class="glyphicon glyphicon-wrench" aria-hidden="true"></span></a>
              <a href="" class="btn btn-danger" data-toggle="modal" data-target="#modalDelete" data-name="{!! $city->name !!}" data-id="{!! $city->id !!}" title="{!! trans('general.tt_delete', ['name' => $city->name]) !!}"><span class="glyphicon glyphicon-remove-circle" aria-hidden="true" ></span></a>
            </td>
          </tr>
        @endforeach
      </tbody>
    </table>
    <div class="text-center">
      {!! $cities->appends(Request::all())->render() !!}
    </div>
  </div>

  <!-- Modal -->
  <div class="modal fade" id="modalDelete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title" id="myModalLabel"></h4>
        </div>
        <div class="modal-body">

        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">{!! trans('general.close') !!}</button>
          <a class="btn btn-danger" href="" id="link">{!! trans('general.delete') !!}</a>
        </div>
      </div>
    </div>
  </div>
@endsection

@section('javascript')
  <script type="text/javascript">
    $('#modalDelete').on('show.bs.modal', function (event) {
      var button = $(event.relatedTarget);
      var name = button.data('name');
      var id = button.data('id');

      var modal = $(this);
      modal.find('.modal-title').text("{!! trans('general.delete_title', ['name' => '" + name + "']) !!}");
      modal.find('.modal-body').text("{!! trans('general.delete_text', ['name' => '" + name + "', 'type' => 'la ciudad de']) !!}");

      var url = "/cities/" + id + "/destroy";

      $("#link").attr("href", url);
    });

    $(function () {
      $('[data-toggle="tooltip"]').tooltip()
      $('[data-toggle="modal"]').tooltip()
    })
  </script>
@endsection
