@extends('template.admin')

@section('title', trans('general.edit_region') . ' ' . $region->name)

@section('content')
  <!-- Form -->
  {!! Form::open(['route' => ['regions.update', $region], 'method' => 'PUT', 'class' => 'form-horizontal']) !!}
    <div class="form-group">
      {!! Form::label('name', trans('general.name'), ['class' => 'col-sm-2 control-label']) !!}
      <div class="col-sm-10">
        {!! Form::text('name', $region->name, ['class' => 'form-control', 'placeholder' => trans('general.ph_name_region'), 'required']) !!}
      </div>
    </div>

    <div class="form-group text-center">
      {!! Form::submit(trans('general.edit_region'), ['class' => 'btn btn-primary']) !!}
    </div>
  {!! Form::close() !!}
@endsection
