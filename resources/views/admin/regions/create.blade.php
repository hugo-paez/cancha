@extends('template.admin')

@section('title', trans('general.insert_region'))

@section('content')
  <!-- Form -->
  {!! Form::open(['route' => 'regions.store', 'method' => 'POST', 'class' => 'form-horizontal']) !!}
    <div class="form-group">
      {!! Form::label('name', trans('general.name'), ['class' => 'col-sm-2 control-label']) !!}
      <div class="col-sm-10">
        {!! Form::text('name', null, ['class' => 'form-control', 'placeholder' => trans('general.ph_name_region'), 'required']) !!}
      </div>
    </div>

    <div class="form-group text-center">
      {!! Form::submit(trans('general.insert_region'), ['class' => 'btn btn-primary']) !!}
    </div>
  {!! Form::close() !!}
@endsection
