@extends('template.admin')

@section('title', trans('general.list_users'))

@section('content')
  <!-- Search -->
  {!! Form::model(Request::all(), ['route' => 'users.index', 'method' => 'GET', 'class' => 'navbar-form']) !!}
    <div class='navbar-left'>
      <div class='input-group'>
        {!! Form::text('name', null, ['class' => 'form-control', 'placeholder' => trans('general.search_user'), 'aria-describedby' => 'search']) !!}
        <span class="input-group-btn">
          {!! Form::button("<span class='glyphicon glyphicon-search' aria-hidden='true'>", array('class' => 'btn btn-search', 'type' => 'submit')) !!}
        </span>
      </div>
    </div>

    <div class='navbar-right'>
      <div class="input-group">
        <a href="{!! route('users.create') !!}" class="btn btn-info">{!! trans('general.insert_user') !!}</a>
      </div>
    </div>
  {!! Form::close() !!}
  <br>
  <hr>

  <!-- Content -->
  <div class="table-responsive">
    <table class="table table-hover">
      <thead>
        <th>{!! trans('general.id') !!}</th>
        <th>{!! trans('general.name') !!}</th>
        <th>{!! trans('general.position') !!}</th>
        <th>{!! trans('general.city') !!}</th>
        <th>{!! trans('general.address') !!}</th>
        <th>{!! trans('general.action') !!}</th>
      </thead>
      <tbody>
        @foreach($users as $user)
          <tr>
            <td>{!! $user->id !!}</td>
            <td>{!! $user->fullname !!}</td>
            <td>{!! $user->type->name !!}</td>
            <td>{!! $user->city->name !!}</td>
            <td>{!! $user->address !!}</td>
            <td>

            </td>
          </tr>
        @endforeach
      </tbody>
    </table>
    <div class="text-center">
      {!! $users->appends(Request::all())->render() !!}
    </div>
  </div>
@endsection
