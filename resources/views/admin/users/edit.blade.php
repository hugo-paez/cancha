@extends('template.admin')

@section('title', trans('general.edit_user') . ' ' . $user->fullname)

@section('css')
  {!! Html::style('plugins/select2/css/select2.min.css') !!}
@endsection

@section('content')
  <!-- Form -->
  {!! Form::open(['route' => ['users.update', $user], 'method' => 'PUT', 'class' => 'form-horizontal']) !!}
    <div class="form-group">
      {!! Form::label('first_name', trans('general.name'), ['class' => 'col-sm-2 control-label']) !!}
      <div class="col-sm-10">
        {!! Form::text('first_name', $user->first_name, ['class' => 'form-control', 'placeholder' => trans('general.ph_first_name'), 'required']) !!}
      </div>
    </div>

    <div class="form-group">
      {!! Form::label('last_name', trans('general.last_name'), ['class' => 'col-sm-2 control-label']) !!}
      <div class="col-sm-10">
        {!! Form::text('last_name', $user->last_name, ['class' => 'form-control', 'placeholder' => trans('general.ph_last_name'), 'required']) !!}
      </div>
    </div>

    <div class="form-group">
      {!! Form::label('email', trans('general.email'), ['class' => 'col-sm-2 control-label']) !!}
      <div class="col-sm-10">
        {!! Form::email('email', $user->email, ['class' => 'form-control', 'placeholder' => trans('general.ph_email'), 'required']) !!}
      </div>
    </div>

    <div class="form-group">
      {!! Form::label('telephone', trans('general.telephone'), ['class' => 'col-sm-2 control-label']) !!}
      <div class="col-sm-10">
        {!! Form::text('telephone', $user->telephone, ['class' => 'form-control', 'placeholder' => trans('general.ph_telephone')]) !!}
      </div>
    </div>

    <div class="form-group">
      {!! Form::label('city_id', trans('general.city'), ['class' => 'col-sm-2 control-label']) !!}
      <div class="col-sm-10">
        {!! Form::select('city_id', $cities, $user->city_id, ['class' => 'form-control', 'placeholder' => trans('general.ph_city'), 'required']) !!}
      </div>
    </div>

    <div class="form-group">
      {!! Form::label('address', trans('general.address'), ['class' => 'col-sm-2 control-label']) !!}
      <div class="col-sm-10">
        {!! Form::text('address', $user->address, ['class' => 'form-control', 'placeholder' => trans('general.ph_address_user'), 'required']) !!}
      </div>
    </div>

    <div class="form-group">
      {!! Form::label('type', trans('general.position'), ['class' => 'col-sm-2 control-label']) !!}
      <div class="col-sm-10">
        {!! Form::select('type_id', $types, $user->type, ['class' => 'form-control', 'placeholder' => trans('general.ph_type'), 'required']) !!}
      </div>
    </div>

    <div class="form-group text-center">
      {!! Form::submit(trans('general.edit_user'), ['class' => 'btn btn-primary']) !!}
    </div>
  {!! Form::close() !!}
@endsection

@section('javascript')
  {!! Html::script('plugins/select2/js/select2.min.js') !!}
  <script type="text/javascript" charset="utf-8">
    $(document).ready(function() {
      $("#city_id").select2({
        placeholder: "{!! trans('general.ph_city') !!}",
        allowClear: true
      });

      $("#type_id").select2({
        placeholder: "{!! trans('general.ph_type') !!}",
        allowClear: true
      });
    });
  </script>
@endsection
