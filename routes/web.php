<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::group(['prefix' => 'admin', 'middleware' => 'auth'], function () {
  Route::resource('users', 'UserController');
  Route::get('users/{users}/destroy', [
    'uses'  =>  'UserController@destroy',
    'as'    =>  'users.destroy'
  ]);

  Route::resource('regions', 'RegionController');
  Route::get('regions/{regions}/destroy', [
    'uses'  =>  'RegionController@destroy',
    'as'    =>  'regions.destroy'
  ]);

  Route::resource('cities', 'CityController');
  Route::get('cities/{cities}/destroy', [
    'uses'  =>  'CityController@destroy',
    'as'    =>  'cities.destroy'
  ]);

  Route::resource('types', 'TypeUserController');
  Route::get('types/{types}/destroy', [
    'uses'  =>  'TypeUserController@destroy',
    'as'    =>  'types.destroy'
  ]);

  Route::resource('reservations', 'ReservationController');
  Route::get('reservations/{reservations}/destroy', [
    'uses'  =>  'ReservationController@destroy',
    'as'    =>  'reservations.destroy'
  ]);

  Route::resource('infrastructures', 'InfrastructureController');
  Route::get('infrastructures/{infrastructures}/destroy', [
    'uses'  =>  'InfrastructureController@destroy',
    'as'    =>  'infrastructures.destroy'
  ]);

  Route::resource('schedules', 'ScheduleController');
  Route::get('schedules/{schedules}/destroy', [
    'uses'  =>  'ScheduleController@destroy',
    'as'    =>  'schedules.destroy'
  ]);
});

Route::get('home', [
  'uses'  =>  'HomeController@index',
  'as'    =>  'home'
]);

Route::get('/', [
  'uses'  =>  'HomeController@index',
  'as'    =>  'root'
]);
