<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Reservation extends Model
{
  /**
   * The table associated with the model.
   *
   * @var string
   */
  protected $table = 'reservations';

  protected $fillable = [
    'user_id', 'infrastructure_id', 'schedule_id', 'date', 'status'
  ];

  public function user()
  {
    return $this->belongsTo(User::class);
  }

  public function infrastructure()
  {
    return $this->belongsTo(Infrastructure::class);
  }

  public function schedule()
  {
    return $this->belongsTo(Schedule::class);
  }
}
