<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Schedule extends Model
{
  /**
   * The table associated with the model.
   *
   * @var string
   */
  protected $table = 'schedules';

  protected $fillable = [
    'start_time', 'end_time'
  ];

  public function reservations()
  {
    return $this->hasMany(Reservation::class);
  }
}
