<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Agreement extends Model
{
  /**
   * The table associated with the model.
   *
   * @var string
   */
  protected $table = 'agreements';

  protected $fillable = [
    'user_id', 'quality'
  ];

  public function user()
  {
    return $this->belongsTo(User::class);
  }

  public function scopeSearch($query, $name)
  {
    return $query->where('user_id', 'LIKE', "%$name%");
  }
}
