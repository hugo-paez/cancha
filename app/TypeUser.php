<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TypeUser extends Model
{
  /**
   * The table associated with the model.
   *
   * @var string
   */
  protected $table = 'type_users';

  protected $fillable = [
    'name'
  ];

  public function users()
  {
    return $this->hasMany(User::class);
  }

  public function scopeSearch($query, $name)
  {
    return $query->where('name', 'LIKE', "%$name%");
  }
}
