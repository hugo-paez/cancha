<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Laracasts\Flash\Flash;
use App\Http\Requests\StoreRegionRequest;
use App\Http\Requests\UpdateRegionRequest;
use App\Region;

class RegionController extends Controller
{
  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index(Request $request)
  {
    $regions = Region::search($request->name)->orderBy('id', 'DESC')->paginate(10);

    return view('admin.regions.index', compact('regions'));
  }

  /**
   * Show the form for creating a new resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function create()
  {
    return view('admin.regions.create');
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function store(StoreRegionRequest $request)
  {
    $region = new Region($request->all());
    $region->save();

    Flash::success('¡Se ha registrado la región ' . $region->name . ' de forma exitosa!');
    return redirect()->route('regions.index');
  }

  /**
   * Display the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function show($id)
  {
    $region = Region::findOrFail($id);

    return view('admin.regions.show', compact('region'));
  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function edit($id)
  {
    $region = Region::find($id);

    return view('admin.regions.edit', compact('region'));
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function update(UpdateRegionRequest $request, $id)
  {
    $region = Region::find($id);
    $regions = $region;
    $region->fill($request->all());
    $region->save();

    Flash::warning('¡Se ha modificado la región ' . $regions->name . ' por ' . $region->name . ' de forma exitosa!');
    return redirect()->route('regions.index');
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function destroy($id)
  {
    $region = Region::find($id);
    $region->delete();

    Flash::error('¡Se ha eliminado la región ' . $region->name . ' de forma exitosa!');
    return redirect()->route('regions.index');
  }
}
