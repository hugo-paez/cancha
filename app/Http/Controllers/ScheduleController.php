<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Laracasts\Flash\Flash;
use App\Http\Requests\StoreScheduleRequest;
use App\Http\Requests\UpdateScheduleRequest;
use App\Schedule;

class ScheduleController extends Controller
{
  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index(Request $request)
  {
    $schedules = Schedule::search($request->start_time)->search($request->end_time)->orderBy('id', 'DESC')->paginate(10);

    return view('admin.schedules.index', compact('schedules'));
  }

  /**
   * Show the form for creating a new resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function create()
  {
    return view('admin.schedules.create');
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function store(StoreScheduleRequest $request)
  {
    $schedule = new Schedule($request->all());
    $schedule->save();

    Flash::success('¡Se ha registrado el horario ' . $schedule->start_time . ' ' . $schedule->end_time . ' de forma exitosa!');
    return redirect()->route('schedules.index');
  }

  /**
   * Display the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function show($id)
  {
    $schedule = Schedule::findOrFail($id);

    return view('admin.schedules.show', compact('schedule'));
  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function edit($id)
  {
    $schedule = Schedule::find($id);

    return view('admin.schedules.edit', compact('schedule'));
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function update(UpdateScheduleRequest $request, $id)
  {
    $schedule = Schedule::find($id);
    $schedule->fill($request->all());
    $schedule->save();

    Flash::warning('¡Se ha modificado el horario ' . $schedule->start_time . ' hasta ' . $schedule->end_time . ' de forma exitosa!');
    return redirect()->route('schedules.index');
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function destroy($id)
  {
    $schedule = Schedule::find($id);
    $schedule->delete();

    Flash::error('¡Se ha eliminado el horario ' . $schedule->start_time .' ' . $schedule->end_time . ' de forma exitosa!');
    return redirect()->route('schedules.index');
  }
}
