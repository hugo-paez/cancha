<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Laracasts\Flash\Flash;
use App\Http\Requests\StoreTypeUserRequest;
use App\Http\Requests\UpdateTypeUserRequest;
use App\TypeUser;

class TypeUserController extends Controller
{
  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index(Request $request)
  {
    $types = TypeUser::search($request->name)->orderBy('id', 'DESC')->paginate(10);

    return view('admin.types.index', compact('types'));
  }

  /**
   * Show the form for creating a new resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function create()
  {
    return view('admin.types.create');
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function store(StoreTypeUserRequest $request)
  {
    $type = new TypeUser($request->all());
    $type->save();

    Flash::success('¡Se ha registrado el tipo de usuario ' . $type->name . ' de forma exitosa!');
    return redirect()->route('types.index');
  }

  /**
   * Display the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function show($id)
  {
    return view('admin.types.show');
  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function edit($id)
  {
    $type = TypeUser::find($id);

    return view('admin.types.edit', compact('type'));
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function update(UpdateTypeUserRequest $request, $id)
  {
    $type = TypeUser::find($id);
    $type->fill($request->all());
    $type->save();

    Flash::warning('¡Se ha modificado el tipo de usuario ' . $type->name . ' de forma exitosa!');
    return redirect()->route('types.index');
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function destroy($id)
  {
    $type = TypeUser::find($id);
    $type->delete();

    Flash::error('¡Se ha eliminado el tipo de usuario ' . $type->name . ' de forma exitosa!');
    return redirect()->route('types.index');
  }
}
