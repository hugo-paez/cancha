<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Laracasts\Flash\Flash;
use App\Http\Requests\StoreReservationRequest;
use App\Http\Requests\UpdateReservationRequest;
use App\Reservation;
use App\User;
use App\Infrastructure;
use App\Schedule;

class ReservationController extends Controller
{
  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index(Request $request)
  {
    $reservations = Reservation::search($request->id)->orderBy('id', 'DESC')->paginate(10);
    $reservations->each(function($reservations){
      $reservations->infrastructure;
      $reservations->user;
      $reservations->schedule;
    });

    return view('admin.reservations.index', compact('reservations'));
  }

  /**
   * Show the form for creating a new resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function create()
  {
    $infrastructures = Infrastructure::orderBy('name', 'ASC')->pluck('name', 'id');
    $users = User::orderBy('name', 'ASC')->pluck('name', 'id');
    $schedules = Schedule::orderBy('name', 'ASC')->pluck('name', 'id');

    return view('admin.reservations.create', compact('users', 'infrastructures', 'schedules'));
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function store(StoreReservationRequest $request)
  {
    $reservation = new Reservation($request->all());
    $reservation->save();
    $reservation->schedules()->sync($request->area);

    Flash::success('¡Se ha registrado la reservación ' . $reservation->id . ' de forma exitosa!');
    return redirect()->route('reservations.index');
  }

  /**
   * Display the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function show($id)
  {
    $reservation = Region::find($id);

    return view('admin.reservations.show', compact('reservation'));
  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function edit($id)
  {
    $reservation = Reservation::findOrFail($id);
    $infrastructures = Infrastructure::orderBy('name', 'ASC')->pluck('name', 'id');
    $users = User::orderBy('name', 'ASC')->pluck('name', 'id');
    $schedules = Schedule::orderBy('name', 'ASC')->pluck('name', 'id');

    return view('admin.reservations.edit', compact('reservation', 'users', 'infrastructures', 'schedules'));
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function update(UpdateReservationRequest $request, $id)
  {
    $reservation = Reservation::find($id);
    $reservation->fill($request->all());
    $reservation->save();

    Flash::warning('¡Se ha modificado la reservación ' . $reservation->id . ' de forma exitosa!');
    return redirect()->route('reservations.index');
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function destroy($id)
  {
    $reservation = Reservation::find($id);
    $reservation->delete();

    Flash::error('¡Se ha eliminado la reservación ' . $reservation->name . ' de forma exitosa!');
    return redirect()->route('reservations.index');
  }
}
