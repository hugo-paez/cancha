<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Laracasts\Flash\Flash;
use App\Http\Requests\StoreCityRequest;
use App\Http\Requests\UpdateCityRequest;
use App\City;
use App\Region;

class CityController extends Controller
{
  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index(Request $request)
  {
    $cities = City::search($request->name)->orderBy('id', 'DESC')->paginate(10);
    $cities->each(function($cities){
      $cities->region;
    });

    return view('admin.cities.index', compact('cities'));
  }

  /**
   * Show the form for creating a new resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function create()
  {
    $regions = Region::orderBy('name', 'ASC')->pluck('name', 'id');

    return view('admin.cities.create', compact('regions'));
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function store(StoreCityRequest $request)
  {
    $city = new City($request->all());
    $city->save();

    Flash::success('¡Se ha registrado la ciudad ' . $city->name . ' de forma exitosa!');
    return redirect()->route('cities.index');
  }

  /**
   * Display the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function show($id)
  {
    $city = City::findOrFail($id);

    return view('admin.cities.show', compact('city'));
  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function edit($id)
  {
    $regions = Region::orderBy('name', 'ASC')->pluck('name', 'id');
    $citiy = City::find($id);

    return view('admin.cities.edit', compact('city', 'regions'));
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function update(UpdateCityRequest $request, $id)
  {
    $city = Area::find($id);
    $city->fill($request->all());
    $city->save();

    Flash::warning('¡Se ha modificado la ciudad ' . $city->name . ' de forma exitosa!');
    return redirect()->route('cities.index');
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function destroy($id)
  {
    $city = City::find($id);
    $city->delete();

    Flash::error('¡Se ha eliminado la ciudad ' . $city->name . ' de forma exitosa!');
    return redirect()->route('cities.index');
  }
}
