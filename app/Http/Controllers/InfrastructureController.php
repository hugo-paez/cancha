<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Laracasts\Flash\Flash;
use App\Http\Requests\StoreInfrastructureRequest;
use App\Http\Requests\UpdateInfrastructureRequest;
use App\Infrastructure;

class InfrastructureController extends Controller
{
  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index(Request $request)
  {
    $infrastructures = Infrastructure::search($request->name)->orderBy('id', 'DESC')->paginate(10);

    return view('admin.infrastructures.index', compact('infrastructures'));
  }

  /**
   * Show the form for creating a new resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function create()
  {
    return view('admin.infrastructures.create');
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function store(StoreInfrastructureRequest $request)
  {
    $infrastructure = new Infrastructure($request->all());
    $infrastructure->save();

    Flash::success('¡Se ha registrado la infrastructura ' . $infrastructure->name . ' de forma exitosa!');
    return redirect()->route('infrastructures.index');
  }

  /**
   * Display the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function show($id)
  {
    $infrastructure = Infrastructure::find($id);

    return view('admin.infrastructures.show', compact('infrastructure'));
  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function edit($id)
  {
    $infrastructure = Infrastructure::findOrFail($id);

    return view('admin.infrastructures.edit', compact('infrastructure'));
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function update(UpdateInfrastructureRequest $request, $id)
  {
    $infrastructure = Infrastructure::find($id);
    $infrastructure->fill($request->all());
    $infrastructure->save();

    Flash::warning('¡Se ha modificado la infrastructura ' . $infrastructure->name . ' de forma exitosa!');
    return redirect()->route('infrastructures.index');
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function destroy($id)
  {
    $infrastructure = Infrastructure::find($id);
    $infrastructure->delete();

    Flash::error('¡Se ha eliminado la infrastructura ' . $infrastructure->name . ' de forma exitosa!');
    return redirect()->route('infrastructures.index');
  }
}
