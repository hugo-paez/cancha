<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Region extends Model
{
  /**
   * The table associated with the model.
   *
   * @var string
   */
  protected $table = 'regions';

  protected $fillable = [
    'name'
  ];

  public function cities()
  {
    return $this->hasMany(City::class);
  }

  public function scopeSearch($query, $name)
  {
    return $query->where('name', 'LIKE', "%$name%");
  }
}
