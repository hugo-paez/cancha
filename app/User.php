<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use DB;

class User extends Authenticatable
{
  /**
   * The table associated with the model.
   *
   * @var string
   */
  protected $table = 'users';

  protected $fillable = [
    'rut', 'first_name', 'last_name', 'email', 'telephone', 'address', 'city_id', 'type_id', 'password'
  ];

  protected $hidden = [
    'password', 'remember_token',
  ];

  public function agreements()
  {
    return $this->hasMany(Agreement::class);
  }

  public function city()
  {
    return $this->belongsTo(City::class);
  }

  public function reservations()
  {
    return $this->hasMany(Reservation::class);
  }

  public function type()
  {
    return $this->belongsTo(TypeUser::class);
  }

  public function scopeSearchRut($query, $rut)
  {
    return $query->where('rut', 'LIKE', "%$rut%");
  }

  public function scopeSearchName($query, $name)
  {
    $name = str_replace(' ', '%', $name);
    return $query->where(DB::raw("CONCAT(first_name, ' ', last_name)"), 'LIKE', "%$name%");
  }

  public function setPasswordAttribute($password)
  {
    $this->attributes['password'] = bcrypt($password);
  }

  public function setFirstNameAttribute($first_name)
  {
    $this->attributes['first_name'] = ucwords($first_name);
  }

  public function setLastNameAttribute($last_name)
  {
    $this->attributes['last_name'] = ucwords($last_name);
  }

  public function setAddressAttribute($address)
  {
    $this->attributes['address'] = ucwords($address);
  }

  public function getFullNameAttribute()
  {
    if(strstr($this->attributes['first_name'], ' ', strlen($this->attributes['first_name'])) != null)
    {
      $first_name = strstr($this->attributes['first_name'], ' ', strlen($this->attributes['first_name']));
    }
    else
    {
      $first_name = $this->attributes['first_name'];
    }

    if(strstr($this->attributes['last_name'], ' ', strlen($this->attributes['last_name'])) != null)
    {
      $last_name = strstr($this->attributes['last_name'], ' ', strlen($this->attributes['last_name']));
    }
    else
    {
      $last_name = $this->attributes['last_name'];
    }
    return $first_name . ' ' . $last_name;
  }

  public function setRutAttribute($rut)
  {
    $separator = ["-", "."];
    $rut = str_replace($separator, '', $rut);

    $this->attributes['rut'] = $rut;
  }
}
