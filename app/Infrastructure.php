<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Infrastructure extends Model
{
  /**
   * The table associated with the model.
   *
   * @var string
   */
  protected $table = 'infrastructures';

  protected $fillable = [
    'name', 'quality', 'capability', 'status'
  ];

  public function scopeSearch($query, $name)
  {
    return $query->where('name', 'LIKE', "%$name%");
  }
}
