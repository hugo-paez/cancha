<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReservationsTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::create('reservations', function (Blueprint $table) {
      $table->increments('id');
      $table->integer('user_id')->unsigned();
      $table->integer('infrastructure_id')->unsigned();
      $table->integer('schedule_id')->unsigned();
      $table->date('date');
      $table->char('status', 1);
      $table->timestamps();

      $table->foreign('user_id')->references('id')->on('users')->OnDelete('cascade');
      $table->foreign('infrastructure_id')->references('id')->on('infrastructures')->OnDelete('cascade');
      $table->foreign('schedule_id')->references('id')->on('schedules')->OnDelete('cascade');
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::dropIfExists('reservations');
  }
}
